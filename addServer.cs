using UnityEngine;
using System.Collections;
using System.Text;
using Mirror;

public class YourNewClassName : NetworkBehaviour {

    private string jsonData;
    [Header("Master Server")]
    public string MasterServerURL;
    public string MasterHeader;
    public string MasterHeaderValue;

  void Start()
  {
    StartCoroutine(PingMasterServer());
  }

  IEnumerator PingMasterServer()
  {
    Debug.Log("Pinging MasterServer");
    jsonData = "{\"ServerName\":\"Local Server2\",\"ServerIP\":\"192.168.0.101\",\"ServerPort\":7777,\"ServerLevel\":\"Training\",\"ServerAdminEmail\":\"alex@cstrike.net\"}";
    UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Post(MasterServerURL, jsonData);
    byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonData);
    www.uploadHandler = (UnityEngine.Networking.UploadHandler)new UnityEngine.Networking.UploadHandlerRaw(bodyRaw);
    www.downloadHandler = (UnityEngine.Networking.DownloadHandler)new UnityEngine.Networking.DownloadHandlerBuffer();
    www.SetRequestHeader("Content-Type", "application/json");
    if(!string.IsNullOrEmpty(MasterHeader) && !string.IsNullOrEmpty(MasterHeaderValue)){
      www.SetRequestHeader(MasterHeader, MasterHeaderValue);
    }
    

    yield return www.SendWebRequest();

    if (www.isNetworkError || www.isHttpError)
    {
      print("Error: " + www.error);
    }
    else
    {
      //at this point the server should be added/updated
      Debug.Log(www.downloadHandler.text);
    }
  }
}  
