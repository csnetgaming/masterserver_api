using UnityEngine;
using SimpleJSON;

public class YourClassName : MonoBehaviour {

  public string MasterServerURL;

  void Start()
  {
    StartCoroutine(LoadServersFromMasterServer());
  }

  IEnumerator LoadServersFromMasterServer()
  {
    if (!string.IsNullOrEmpty(MasterServerURL))
    {

      WWW www = new WWW(MasterServerURL);
      yield return www;

      if (string.IsNullOrEmpty(www.error))
      {
        JSONNode jsonNode = JSON.Parse(www.text);

        if (jsonNode["Status"] == "failed")
        {
            yield return null;
        }

        for (int i = 0; i < jsonNode["ServerCount"]; ++i)
        {
          Debug.Log("SERVER FOUND " + jsonNode["Servers"][i]["ServerIP"]);
          string ServerHostname = jsonNode["Servers"][i]["ServerName"];
          string ServerIP = jsonNode["Servers"][i]["ServerIP"];
          string ServerPort = jsonNode["Servers"][i]["ServerPort"];
          string ServerLevel = jsonNode["Servers"][i]["ServerLevel"];
          string ServerAdminEmail = jsonNode["Servers"][i]["ServerAdminEmail"];
          string ServerOnlineSince = jsonNode["Servers"][i]["AddedTimestamp"];
        }
      }
      else
      {
        Debug.LogError("HTTP ERROR");
      }
    }
    else
    {
      Debug.LogError("Please set the URL to fetch data");
    }
  }
}  
