## Simple Master Server
---
Compile the master server for your desired OS:

* Download GO from https://golang.org/dl/ , other installation instructions can be found at https://golang.org/doc/install
* After GO's installation open a command prompt/terminal and type : 
```go env```

* Check the GOPATH value and switch to that folder.
* Clone this repo inside it or just create a folder and copy the masterserver.go and config.toml inside it.
* Fetch all the required resources using ```go get -v```
* After all the resources are fetched, build it with ```go build```

The output will be an executable file built for your current operating system ( i.e. .exe for Lindows users ).

Don't forget to create a mysql database name and add credentials into the config.toml.

The required table name will be created if does not exist. Feel free to change his name inside the config file.

The config file contains info about most of the settings but will be updated to cover all of them. 

### Endpoints 

>/addServer

This endpoint will only accept json data sent with POST.

Sample json :
```
{
    "ServerName":"Local Server",
    "ServerIP":"192.168.0.101",
    "ServerPort":7777,
    "ServerLevel":"Training",
    "ServerAdminEmail":"admin@example.com"
}
```
The API will send a json formated error message if there is any issue with your query or the success message when a server is added.
Failure message sample:
```
{
    "Status":"failed",
    "Message":"Query server and IP do not match"
}
```

Success message sample:
```
{
    "Status":"Success",
    "Message":"Server was added/updated"
}
```

>/getServers 

This endpoint will return an array containing the status of the query, the count of total found servers and a multidimensional array containing data about each server.

Sample output:
```
{
    "Status": "Success",
    "ServerCount": 1,
    "Servers": [
        {
            "ServerName": "Local Server2",
            "ServerIP": "192.168.0.101",
            "ServerPort": 7777,
            "ServerLevel": "Training",
            "ServerAdminEmail": "admin@example.com",
            "AddedTimestamp": 1539035265
        }
    ]
}
```
**Both addServer.cs and fetchServers.cs contain code snippets which can provide an idea of how to implement this with Unity.** 

While this is not a complete integration with Unity, it works if the codes are added on the right places.

