package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
	_ "github.com/go-sql-driver/mysql"
)

// Config structure contains all the dynamic data
// Make sure to type structure types correctly
type Config struct {
	Webserver    webserver
	Security     security
	Database     database
	MasterServer masterserver
}

type masterserver struct {
	AllowRestrictedPorts      bool `toml:"AllowRestrictedPorts"`
	MinAllowedPort            int  `toml:"MinAllowedPort"`
	RequireAdminEmail         bool `toml:"RequireAdminEmail"`
	RequireServerName         bool `toml:"RequireServerName"`
	MaxCharactersInServerName int  `toml:"MaxCharactersInServerName"`
	RequireLevelName          bool `toml:"RequireLevelName"`
}

type webserver struct {
	Bind           string `toml:"Bind"`
	Port           int    `toml:"Port"`
	SslEnabled     bool   `toml:"SSL"`
	SslPort        int    `toml:"SSL_port"`
	SslCertificate string `toml:"SSL_certificate_key"`
	SslPrivateKey  string `toml:"SSL_private_key"`
}

type security struct {
	AccessType           string   `toml:"AccessType"`
	AllowedIP            []string `toml:"AllowedIP"`
	AllowedHeader        string   `toml:"AllowedHeader"`
	AllowedHeaderContent string   `toml:"AllowedHeaderContent"`
}

type database struct {
	DatabaseServer   string `toml:"DatabaseHost"`
	DatabaseUser     string `toml:"DatabaseUser"`
	DatabasePassword string `toml:"DatabasePassword"`
	DatabaseName     string `toml:"DatabaseName"`
	DatabasePort     int    `toml:"DatabasePort"`
	ServersTableName string `toml:"ServersTableName"`
}

type gameServer struct {
	ServerName       string `json:"ServerName"`
	ServerIP         string `json:"ServerIP"`
	ServerPort       int    `json:"ServerPort"`
	ServerLevel      string `json:"ServerLevel"`
	ServerAdminEmail string `json:"ServerAdminEmail"`
}

type serverList struct {
	Status      string
	ServerCount int
	Servers     []*servers
}

type servers struct {
	ServerName       string
	ServerIP         string
	ServerPort       int
	ServerLevel      string
	ServerAdminEmail string
	AddedTimestamp   int32
}

// Response ...
type Response struct {
	Status  string
	Message string
}

func makeResponse(Status string, Message string) []byte {
	response := Response{Status, Message}
	encodedResponse, err := json.Marshal(response)
	if err != nil {
		fmt.Println(err)
	}
	return encodedResponse
}

/*
	We will define some variables here to keep them global.
	Not a good idea if there are any other packages which contain the same variable names
	Make sure to keep unique variable names.
*/
var conf Config
var db *sql.DB

func main() {
	LoadMasterServerConfiguration()

	db = LoadMysqlDatabase()
	defer db.Close()
	createServersTable()
	/**
	Fetch server list
	*/
	http.HandleFunc("/getServers", getServers)
	/**
	Fetch server list
	*/
	http.HandleFunc("/addServer", addServer)
	/**
	Start our webserver
	*/
	server := fmt.Sprintf("%s:%d", conf.Webserver.Bind, conf.Webserver.Port)
	log.Fatal(http.ListenAndServe(server, nil))
	fmt.Println("Loaded")
}

func addServer(rw http.ResponseWriter, req *http.Request) {

	fmt.Println("new request from " + req.RemoteAddr)
	//security
	if !ValidClient(req.RemoteAddr, req.Header.Get(conf.Security.AllowedHeader)) {
		fmt.Println("Security fail " + req.Header.Get(conf.Security.AllowedHeader))
		http.Error(rw, "Missing/Wrong security headers", http.StatusUnauthorized)
		return
	}
	if req.Method != "POST" {

		fmt.Println("no post from " + req.RemoteAddr)
		http.Error(rw, "No POST, no FUN", http.StatusUnauthorized)
		return
	}

	decoder := json.NewDecoder(req.Body)
	decoder.UseNumber()
	var server gameServer
	err := decoder.Decode(&server)
	if err != nil {
		fmt.Println("decode err " + req.RemoteAddr)
		b, err := ioutil.ReadAll(req.Body)
		req.Body.Close()
		if err != nil {
			fmt.Fprintln(os.Stderr, "[LOW] Body fetch issue", err)
		}
		fmt.Println(string(b))

		http.Error(rw, string(makeResponse("failed", "Invalid data provided")), http.StatusUnauthorized)
		return
	}
	if conf.MasterServer.RequireServerName == true {
		if len(server.ServerName) == 0 {
			http.Error(rw, string(makeResponse("failed", "Server name is required")), http.StatusNotAcceptable)
			return
		}
	}
	//Make sure that the server ip is the same with caller's ip
	if len(server.ServerIP) == 0 {
		return
	}

	ip := strings.Split(req.RemoteAddr, ":")
	if strings.Compare(ip[0], server.ServerIP) != 0 {

		http.Error(rw, string(makeResponse("failed", "Query server and IP do not match")), http.StatusNotAcceptable)
		return
	}

	// Ports less than 1024 are reserved to system and can be used by root only.
	// Spawning a server as root is not recommended
	if conf.MasterServer.AllowRestrictedPorts == false {
		if server.ServerPort <= conf.MasterServer.MinAllowedPort {
			http.Error(rw, string(makeResponse("failed", "Servers running restricted ports are not allowed")), http.StatusNotAcceptable)
			return
		}
	}

	if conf.MasterServer.RequireAdminEmail == true {
		if len(server.ServerAdminEmail) == 0 {
			http.Error(rw, string(makeResponse("failed", "Admin email is required")), http.StatusNotAcceptable)
			return
		}
	}

	if conf.MasterServer.RequireLevelName == true {
		if len(server.ServerLevel) == 0 {
			http.Error(rw, string(makeResponse("failed", "Server running scene is required")), http.StatusNotAcceptable)
			return
		}
	}
	currentTime := int32(time.Now().Unix())
	query := fmt.Sprintf("INSERT INTO `%s` (`serverName`,`serverIP`,`serverPort`,`serverLevel`,`adminEmail`,`added`) VALUES ('%s', '%s', '%d', '%s', '%s','%d') ON DUPLICATE KEY UPDATE serverName = values(serverName), serverIP = values(serverIP),serverPort = values(serverPort), serverLevel = values(serverLevel), adminEmail = values(adminEmail), added = values(added)", conf.Database.ServersTableName, server.ServerName, server.ServerIP, server.ServerPort, server.ServerLevel, server.ServerAdminEmail, currentTime)
	answer, err := db.Query(query)
	if err != nil {
		panic(err)
	}
	answer.Close()
	response := &Response{"Success", "Server was added/updated"}
	data, err := json.Marshal(response)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(200)
	rw.Header().Set("Content-Type", "application/json")
	rw.Write(data)
}

func getServers(rw http.ResponseWriter, req *http.Request) {
	list := make([]*servers, 0, 0)

	query := fmt.Sprintf("SELECT * FROM `%s` ORDER BY added DESC LIMIT 100", conf.Database.ServersTableName)
	rows, err := db.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	var serverName string
	var serverIP string
	var serverPort int
	var serverLevel string
	var adminEmail string
	var addedTimestamp int32

	for rows.Next() {
		err = rows.Scan(&serverName, &serverIP, &serverPort, &serverLevel, &adminEmail, &addedTimestamp)
		if err != nil {
			panic(err)
		}
		list = append(list, &servers{serverName, serverIP, serverPort, serverLevel, adminEmail, addedTimestamp})
	}
	serverCount := len(list)
	answer := &serverList{
		Status:      "Success",
		ServerCount: serverCount,
		Servers:     list,
	}
	data, err := json.Marshal(answer)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(200)
	rw.Header().Set("Content-Type", "application/json")
	rw.Write(data)
}

// LoadMasterServerConfiguration will load the main config file so we can use
// data from it in places where we need it.
// This function must be called first since rest of the functions will depend on it.
func LoadMasterServerConfiguration() {
	if _, err := os.Stat("./.dev"); err == nil {

		if _, err := toml.DecodeFile("./config-dev.toml", &conf); err != nil {
			fmt.Println(err)
		}
	} else {
		if _, err := toml.DecodeFile("./config.toml", &conf); err != nil {
			fmt.Println(err)
		}
	}
	fmt.Println("Configuration has been loaded")
}

func createServersTable() {
	query := fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (`serverName` text NOT NULL,`serverIP` varchar(15) NOT NULL,`serverPort` int(5) NOT NULL DEFAULT '7777',`serverLevel` text NOT NULL,`adminEmail` text NOT NULL,`added` int(10) NOT NULL,PRIMARY KEY (`serverIP`))", conf.Database.ServersTableName)
	answer, err := db.Query(query)
	if err != nil {
		panic(err)
	}
	answer.Close()
}

// LoadMysqlDatabase is enableing a MySQL connection which we can use to store/fetch servers.
func LoadMysqlDatabase() *sql.DB {

	connection := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", conf.Database.DatabaseUser, conf.Database.DatabasePassword, conf.Database.DatabaseServer, conf.Database.DatabasePort, conf.Database.DatabaseName)
	db, err := sql.Open("mysql", connection)
	if err != nil {
		fmt.Println("Sql error")
		panic(err)
	}
	fmt.Println("Connected to MySQL")
	return db
}

// IsPublicIP will validate an IP Address to make sure that is public.
// There is no point in allowing local networks.
func IsPublicIP(Address *net.IPNet) bool {
	if Address.IP.IsLoopback() || Address.IP.IsLinkLocalMulticast() || Address.IP.IsLinkLocalUnicast() {
		return false
	}
	if ip4 := Address.IP.To4(); ip4 != nil {
		switch true {
		case ip4[0] == 10:
			return false
		case ip4[0] == 172 && ip4[1] >= 16 && ip4[1] <= 31:
			return false
		case ip4[0] == 192 && ip4[1] == 168:
			return false
		default:
			return true
		}
	}
	return false
}

// ValidClient function ...
func ValidClient(RemoteAddr string, HeaderContent string) bool {

	switch accessMethod := conf.Security.AccessType; accessMethod {
	case "PermitAll":
		{
			return true
		}
	case "PermitIP":
		{
			isAllowed := false
			for _, allowedIP := range conf.Security.AllowedIP {
				fmt.Println("Current IP " + RemoteAddr + " Allowed " + allowedIP)
				s := strings.Split(RemoteAddr, ":")
				ip := s[0]
				if strings.Compare(ip, allowedIP) == 0 {
					isAllowed = true
					break
				}
			}
			return isAllowed
		}
	case "PermitHeader":
		{
			/*
				Check if header is set and has the correct value
			*/
			if strings.Compare(HeaderContent, conf.Security.AllowedHeaderContent) == 0 {
				return true
			}
			return false
		}
	}
	return false
}
